## v1.15.1 (2023-05-25)

### New features

- Update GitLab Runner version to v16.0.1

## v1.15.0 (2023-05-22)

### New features

- Update GitLab Runner version to v16.0.0

### Maintenance

- Upgrade versions !136
- Adapt to new token architecture !134

## v1.14.0 (2023-04-22)

### New features

- Update GitLab Runner version to v15.11.0

### Bug fixes

- Fix applying previous version in csv !133
- Fix published operator path in GitHub !132

## v1.13.0 (2023-03-31)

### New features

- Update GitLab Runner version to v15.10.1

### Bug fixes

- Fix applying previous version in csv !133
- Fix published operator path in GitHub !132

### Maintenance

- Add automated GitHub release, move kube-rbac-proxy image to GitLab Registry, useSkipRange and replaces in final CSV, automatically update final CSV createdAt field, automatically set Openshift versions annotations !131
- Set security context properties to operator managed containers to suppress warnings !130

## v1.12.0 (2023-02-05)

### New features

- Update GitLab Runner version to v15.8.1 

### Bug fixes

- Improve error handling and processing around release.yaml !129

### Maintenance

- Bump supported OpenShift version to v4.12 !128

## v1.11.0 (2022-12-22)

#### Upstream Release Pull Requests

- RedHat Certified Operators https://github.com/redhat-openshift-ecosystem/certified-operators/pull/1697
- RedHat Community Operators https://github.com/redhat-openshift-ecosystem/community-operators-prod/pull/1999
- Kubernetes Community Operators https://github.com/k8s-operatorhub/community-operators/pull/2088

### New Features

- Update GitLab Runner version to v15.7.1

### Maintenance

- Report errors in CR status !116 (m-terra @m-terra)
- Change interpreter for register-runner to BASH !120
- Enhance operator label to avoid conflicts !118

## v1.10.0 (2022-08-08)

### New Features

- Update GitLab Runner version to v15.2.1

### Bug fixes

- Fix digests in related images and release config for certified images by using skopeo !117

## v1.9.0 (2022-05-31)

### New Features

- Update GitLab Runner version to v15.0.0

## v1.8.0 (2022-04-24)

### New Features

- Update GitLab Runner version to v14.10.0
- Add runner registration related properties !105 (Em Karisch @QuingKhaos)

### Bug Fixes

- Sort user provided/custom environments variables from config map !104 (Em Karisch @QuingKhaos)

### Maintenance

- Add tests for testing runner_controller and in particular sorting/fetching of env variables !107
- DRY k8s bundle, update CSV description to include new docs link and current Runner version !113
- Update controller gen objects !110
- Check controller-gen objects & fix CI image !112
- Update the controller-runtime Client we use for mocks to the latest interface version !109
- Update to controller-runtime@v0.7.2 !103 (Em Karisch @QuingKhaos)
- Automate installation of development tools !102

## v1.7.0 (2022-03-23)

### New features

- Add Kubernetes support for Operator !97
- Allow changing runner image !79
  
### Maintanence

- Update to Go version 1.17 !101
- Bump GitLab Runner version to v14.8.2

## v1.6.0 (2022-02-23)

### Maintenance

- Replace feature::maintenance with type::maintenance !99
- Add undeploy to makefile to match standard Makefile from operator-sdk !96 (Philip Schwartz @pschwar1)
- Bump GitLab Runner version to v14.7.0

### Other changes

- Add Community multiarch support !89

## v1.5.0 (2021-10-08)

### Maintanance

- Bump GitLab Runner version to v14.3.0
- Add support for OpenShift v4.9

## v1.4.0 (2021-09-28)

### Documentation changes

- Adding CONTRIBUTING.md to gitlab-runner-operator after license audit review. !86

### Maintanance

- Bump GitLab Runner version to v14.2.0

### Other changes

- enable security tools !87

## v1.3.0 (2021-08-16)

### Maintenance

- Bump GitLab Runner version to v14.1.0

## v1.2.0 (2021-07-01)

### New features

- add disconnected environment annotation !80 (Edmund Ochieng @ochienged)

### Maintenance

- Create Community operator resources !74
- Push bundle increment postfix !81

### Other changes

- Update README.md !83

## v1.1.0 (2021-04-22)

### New features

- Support cluster wide operator and cleanup resources !73

### Maintenance

- Don't fail jobs when docker images already exist !77
- Add Mockery and testify packages. Vendor dependencies !76
- Refactor runner controller to allow for easier testing !78

## v1.0.0 (2021-03-22)

### New features

- Allow specifying ImagePullPolicy !55
- set CI_SERVER_TLS_CA_FILE environment variable !52 (Edmund Ochieng @ochienged)

### Maintenance

- Add changelog generation !60
- Update UBI images to latest version to fix helper path !59
- Improve docs and build !57
- refactor registration script !56 (Edmund Ochieng @ochienged)
- refactor registration.sh script !53 (Edmund Ochieng @ochienged)
- Set CSV version and replaced version automatically during the bundle build based on existing tags !51
- Build images not only for tags !50
- Remove and gitignore files touched during bundling and automate related_images creation !49
- Move and improve images building docs. Automate release.yaml creation !48
- use CI_REGISTRY variable !47 (Edmund Ochieng @ochienged)
- Automatic build and deployment of images to RedHat connect !42

### Documentation changes

- Add support and maintenance section !54
- fix docs for release.yaml location !46
